### Fetch JSON using Axios with TypeScript Compiler

#### Installation
To install the package dependencies.
```
npm install
```

#### Package Dependencies
```
npm install axios ts-node typescript
```

`Axios`<br>
Axios is a promised based HTTP client for the browser and node.js. <br >
For more information please visit: 
https://github.com/axios/axios
<br><br>

`TypeScript`<br>
TypeScript is a JavaScript compiler. Think of it as JavaScript with Additional syntax. Also with this compiler it will tell
you right away if you are doing bad code.<br>
For more information please visit: https://github.com/Microsoft/TypeScript
<br><br>

`ts-node`<br>
ts-node is an npm package that compiles TypeScript file and
run node in async.<br >
For more information please visit: https://github.com/Microsoft/TypeScript

#### Additional Resources 
JSON Placeholder for fake data <br>
http://jsonplaceholder.typicode.com/
<br >
Grabbing the the first todo.<br >
http://jsonplaceholder.typicode.com/todos/1
```
{
  "userId": 1,
  "id": 1,
  "title": "delectus aut autem",
  "completed": false
}
```

`tsc` stands for TypeScript compiler. What it does is it compiles the typescript file into
a JavaScript file.
```
tsc index.ts => compiles => index.js
```

`node` is used to run our JavaScript file. So the command would be `node index.js`. <br>

`ts-node` is used to compile TypeScript files and run it as JavaScript. <br>
```
ts-node index.ts
```
#### Output
```
{ userId: 1, id: 1, title: 'delectus aut autem', completed: false }
```